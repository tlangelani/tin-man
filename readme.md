# tinMan App

Demo: http://142.93.103.175
Admin: http://142.93.103.175/admin

Email: john.doe@gmail.com
Pass: system2013

This project requires the following packages to be installed on setup machine:
- PHP 7
- MySQL
- Node.js
- Composer

It is written using the following technologies:
- Laravel 5.5 (Backend)
- Vue.js (Frontend)
- Sass

To get started:

1. Install PHP dependencies: `composer install`
2. Install Node packages: `npm install`
3. Copy `.env.example` to `.env` and update database credentials
4. Create tables: `php artisan migrate`
5. Create symbolic link from `storage` to `public/storage`: `php artisan storage:link` (this is to allow access to uploaded images to be accessible publicly)
6. Build assets for dev or production: `npm run dev` / `npm run prod`
7. To access the site you can either setup apache virtual host to point to `public` directory or just run `php artisan serve`

If you experience any issues feel free to contact:
mbowenit@gmail.com (0734587031)
