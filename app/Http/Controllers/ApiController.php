<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getProducts()
    {
        $products = Product::with('category')->get();
        return $products;
    }

    public function getCategories()
    {
        $categories = Category::all();
        return $categories;
    }
}
