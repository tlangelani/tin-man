<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function all()
    {
        $categories = Category::all();
        return view('dashboard.categories.index')->with('categories', $categories);
    }

    public function create()
    {
        return view('dashboard.categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $category = new Category();
        $category->name = $request->input('name');

        $category->save();

        return redirect('/admin/categories')->with('success', 'Category Added!');
    }

    public function edit($id)
    {
        $category = Category::find($id);

        return view('dashboard.categories.edit')->with('category', $category);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $category = Category::find($id);
        $category->name = $request->input('name');

        $category->save();

        return redirect('/admin/categories')->with('success', 'Category Updated!');
    }

    public function destroy($id)
    {
        $category = Category::find($id);

        $category->delete();

        return redirect('/admin/categories')->with('success', 'Category Deleted!');
    }
}
