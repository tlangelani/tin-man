<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['all', 'create', 'store', 'edit', 'update']]);
    }

    public function index()
    {
        return view('catalog.index');
    }

    public function all()
    {
        $products = Product::with('category')->get();

        return view('dashboard.products.index')->with('products', $products);
    }

    public function create()
    {
        $categories = Category::all();
        return view('dashboard.products.create')->with('categories', $categories);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'image' => 'image|max:1999',
            'category_id' => 'required'
        ]);

        $originalFilename = $request->file('image')->getClientOriginalName();
        $basename = pathinfo($originalFilename, PATHINFO_FILENAME);
        $extension = pathinfo($originalFilename, PATHINFO_EXTENSION);

        // create new filename
        $filename = $basename.'_'.time().'.'.$extension;

        // upload image
        $path = $request->file('image')->storeAs('public/products/', $filename);

        $product = new Product();
        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->image = $filename;
        $product->category_id = $request->input('category_id');

        $product->save();

        return redirect('/admin/products')->with('success', 'Product Added!');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();

        return view('dashboard.products.edit')->with('product', $product)->with('categories', $categories);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'image' => 'image|max:1999',
            'category_id' => 'required'
        ]);

        $product = Product::find($id);
        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->category_id = $request->input('category_id');

        $product->save();

        return redirect('/admin/products')->with('success', 'Product Updated!');
    }
}
