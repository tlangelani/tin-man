<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// get all products
Route::get('/products', 'ApiProductsController@index');

// get all categories
Route::get('/categories', 'ApiCategoriesController@index');
// list single category
Route::get('/category/{id}', 'ApiCategoriesController@show');
// create new category
Route::post('/category', 'ApiCategoriesController@store');
// update category
Route::put('/category', 'ApiCategoriesController@store');
// delete category
Route::delete('/category/{id}', 'ApiCategoriesController@destroy');
