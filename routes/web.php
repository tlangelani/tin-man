<?php

Route::get('/', function () {
    return redirect('/catalog');
});

Route::get('/catalog', 'ProductsController@index')->name('catalog');

Auth::routes();

/**
 * Dashboard
 */
Route::get('/admin', 'DashboardController@index')->name('dashboard');

Route::get('/admin/products', 'ProductsController@all')->name('admin.products');
Route::get('/admin/product/create', 'ProductsController@create');
Route::post('/admin/product/store', 'ProductsController@store');
Route::get('/admin/product/edit/{id}', 'ProductsController@edit');
Route::put('/admin/product/update/{id}', 'ProductsController@update');
Route::delete('/admin/product/{id}', 'ProductsController@destroy');

Route::get('/admin/categories', 'CategoriesController@all')->name('admin.categories');
Route::get('/admin/category/create', 'CategoriesController@create');
Route::post('/admin/category/store', 'CategoriesController@store');
Route::get('/admin/category/edit/{id}', 'CategoriesController@edit');
Route::put('/admin/category/update/{id}', 'CategoriesController@update');
Route::delete('/admin/category/{id}', 'CategoriesController@destroy');

