<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>tinMan | Dashboard</title>
    <link rel="stylesheet" href="{{ asset('/css/admin.css') }}">
</head>
<body>
    <div id="app">
        @include('partials.admin.nav')
        @include('partials.admin.header')
        @include('partials.messages')
        @yield('content')
    </div>
    <script src="{{asset('/js/app.js')}}"></script>
</body>
</html>
