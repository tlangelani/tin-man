<div id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <small>Manage your site</small></h1>
            </div>
            <div class="col-md-2">
                <div class="dropdown pull-right">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Create Content
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="/admin/product/create">Add Product</a></li>
                        <li><a href="/admin/category/create">Add Category</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
