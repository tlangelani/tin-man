@extends('layouts.dashboard')

@section('content')
    <div class="container categories">
        <h1>Categories</h1>
        @if(count($categories) > 0)
            <ul class="list-group">
            @foreach($categories as $category)
                <li class="list-group-item">
                    {{$category->name}}
                    <div class="actions pull-right">
                        <a href="/admin/category/edit/{{$category->id}}" class="btn btn-info btn-xs edit" role="button">
                            <span class="glyphicon glyphicon-pencil pull-left"></span>&nbsp;EDIT
                        </a>
                        <form action="/admin/category/{{$category->id}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger btn-xs">
                                <span class="glyphicon glyphicon-remove"></span> DELETE
                            </button>
                        </form>
                    </div>
                </li>
            @endforeach
            </ul>
        @else
            <p class="alert alert-warning">There are no categories!</p>
        @endif
    </div>
@endsection
