@extends('layouts.dashboard')

@section('content')
    <div class="container">
        <h1>Edit Category</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Category / Edit</h3>
                    </div>
                    <div class="panel-body">
                        <form action="/admin/category/update/{{$category->id}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="put">
                            <div class="form-group">
                                <label for="name">Category Name</label>
                                <input class="form-control" type="text" id="name" name="name" value="{{$category->name}}">
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
