@extends('layouts.dashboard')

@section('content')
    <div class="container">
        <h1>Create Category</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Category / Add</h3>
                    </div>
                    <div class="panel-body">
                        <form action="/admin/category/store" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Category Name</label>
                                <input class="form-control" type="text" id="name" name="name" placeholder="Category Name">
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
