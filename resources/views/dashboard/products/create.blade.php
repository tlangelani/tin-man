@extends('layouts.dashboard')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Create Product</h1>
                <form action="/admin/product/store" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" type="text" id="title" required name="title" placeholder="Product Title">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" rows="3" name="description" required id="description" placeholder="Product Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <div class="input-group">
                            <div class="input-group-addon">R</div>
                            <input class="form-control" type="text" id="price" name="price" required placeholder="Product Price">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image">Image (size: 355x355)</label>
                        <input class="form-control-file" type="file" id="image" name="image" placeholder="Product Image">
                    </div>
                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select name="category_id" id="category_id" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
    </div>
@endsection
