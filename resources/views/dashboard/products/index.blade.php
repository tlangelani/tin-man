@extends('layouts.dashboard')

@section('content')
    <div class="container products">
        <h1>Products</h1>
        @if(count($products) > 0)
            <div class="row">
            @foreach($products as $product)
                <div class="col-md-3 col-sm-3">
                    <section class="product">
                        <div class="thumbnail">
                            <div class="image-container" style="background-image: url('/storage/products/{{$product->image}}')"></div>
                            <img style="display: none" src="/storage/products/{{$product->image}}" alt="{{$product->title}}">
                            <div class="caption">
                                <h3>{{$product->title}} <span class="label label-warning pull-right">{{$product->category->name}}</span></h3>
                                <p>{{$product->description}}</p>
                                <p class="price">{{$product->price}}</p>
                                <div class="actions">
                                    <a href="/admin/product/edit/{{$product->id}}" class="btn btn-info edit" role="button">
                                        <span class="glyphicon glyphicon-pencil pull-left"></span>&nbsp;EDIT
                                    </a>
                                    <form action="/admin/product/{{$product->id}}" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-remove"></span> DELETE
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            @endforeach
            </div>
        @else
            <p class="alert alert-warning">There are no products! <a href="/admin/category/create"></a></p>
        @endif
    </div>
@endsection
