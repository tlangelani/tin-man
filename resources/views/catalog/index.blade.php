@extends('layouts.master')

@section('content')
    <div class="container">
        <h1>Catalog</h1>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis deleniti facere illum nihil rerum voluptate! Accusamus quaerat soluta voluptates.
        </p>
        <div class="row">
            @foreach(range(1, 4) as $index)
                <div class="col-md-3 col-sm-3">
                    <section class="product">
                        <div class="thumbnail">
                            <img src="https://via.placeholder.com/250x300.jpg" alt="...">
                            <div class="caption">
                                <h3>Product Name</h3>
                                <p>Product Description</p>
                                <p class="price">R499</p>
                                <p class="actions">
                                    <a href="#" class="btn btn-info" role="button">Add to Cart</a>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>
            @endforeach
        </div>
    </div>
@endsection
